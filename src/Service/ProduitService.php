<?php

namespace App\Service;

use App\Entity\Produits;
use App\Repository\ProduitsRepository;
use Throwable;

class ProduitService
{

    // injection de dépendance de ProduitsRepository
    public function __construct(private ProduitsRepository $repository)
    {
    }

    // méthode permettant la récupération de tout les produits dans le repository
    public function getAll(): array
    {
        // je viens récupérer les datas depuis la méthode finAll() du repository
        return $this->repository->findAll();
    }

    // méthode permettant de récupérer un produit par son id
    // l'id est récupérée depuis le controller
    /**
     * @throws Exception
     */
    public function getById(int $id): Produits | string
    {
        // on interroge le repository
        $data = $this->repository->find($id);

        // si le repo ne retourne pas de produit
        if ($data == null) {
           return "id inconnue";
        }
        // sinon on retourne le produit
        return $data;
    }

    /**
     * @param $data
     * @return Produits|string|null
     * méthode permettant la creation d'un produit
     * retourne le produit créé ou une erreur
     * dans le cas ou le flush n'est pas précisé, retourne null
     */
    public function create($data): Produits | string | null
    {
        $produit = new Produits();
        $produit->setNom($data->nom);
        $produit->setDescription($data->description);
        $produit->setPrix($data->prix);

        try {
            return $this->repository->save($produit, true);
        } catch (Throwable $e)
        {
            return $e->getMessage();
        }
    }

    /**
     * @param $data
     * @param Produits $produit
     * @return Produits|string|null
     * méthode permettant la mise à jour d'un produit
     * retourne l'objet mis à jour
     */
    public function update($data, Produits $produit): Produits | string | null
    {
        $produit->setNom($data->nom);
        $produit->setDescription($data->description);
        $produit->setPrix($data->prix);

        try {
            return $this->repository->save($produit, true);
        } catch (Throwable $e)
        {
            return $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @param float $prix
     * @return Produits|string
     * méthode permettant la mise à jour du prix d'un produit
     * retourne le produit modifié
     */
    public function updatePrice(int $id, float $prix): Produits | string
    {
        try {
            return $this->repository->changePrice($id, $prix);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param Produits $produit
     * @return void
     * méthode permettant la suppression d'un produit
     * ne retourne rien
     */
    public function delete(Produits $produit): void
    {
        $this->repository->remove($produit, true);
    }
}