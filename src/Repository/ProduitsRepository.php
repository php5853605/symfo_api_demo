<?php

namespace App\Repository;

use App\Entity\Produits;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use ErrorException;

/**
 * @extends ServiceEntityRepository<Produits>
 *
 * @method Produits|null find($id, $lockMode = null, $lockVersion = null)
 * @method Produits|null findOneBy(array $criteria, array $orderBy = null)
 * @method Produits[]    findAll()
 * @method Produits[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProduitsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Produits::class);
    }

    public function save(Produits $entity, bool $flush = false): Produits | null
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
            return $entity;
        }

        return null;
    }

    public function remove(Produits $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * méthode permettant la mise à jour du champ prix d'un produit
     * @param int $id
     * @param float $prix
     * @throws ErrorException
     */
    public function changePrice(int $id, float $prix): Produits
    {
        // j'utilize la méthode querybuilder pour définir une requête custom
        // la méthode renvoie un boolean
        $value = $this->createQueryBuilder('p')
            // je précise l'entité sur laquelle je vais travailler
            ->update(Produits::class, 'p')
            // je défini les champs à mettre à jour
            ->set('p.prix', ':prix')
            // je précise le produit à mettre à jour via son id
            ->where('p.id = :id')
            // je set les valeurs
            ->setParameter('prix', $prix)
            ->setParameter('id', $id)
            // je récupère la query sql
            ->getQuery()
            // j'execute la query
            ->execute();

        if ($value == 1) {
            return $this->find($id);
        }
        throw new ErrorException("la mise à jour n'a pu se faire car le prix n'a pas changé");
    }

//    /**
//     * @return Produits[] Returns an array of Produits objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Produits
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
